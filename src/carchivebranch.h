/*
	*
	* This file is a part of CoreArchiver.
	* Archive manager for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#pragma once

#include <QObject>
#include <QIcon>

class CArchiveBranch : public QObject {
	Q_OBJECT

public:
	explicit CArchiveBranch();
	explicit CArchiveBranch(const QString &, QIcon, const QString &, CArchiveBranch *parent = 0);

	~CArchiveBranch();

	int branchCount();

	void clearBranches();

	void addBranch(CArchiveBranch *);
	void removeBranch(CArchiveBranch *);

	CArchiveBranch *branch(int);
	CArchiveBranch *branch(const QString &);
	QList<CArchiveBranch *> branches();

	QVariant data(int role) const;
	bool setData(int column, QVariant data);

	CArchiveBranch *parent();
	int row();

	void sort();

	QString name();
	QString size();
	QIcon icon();

private:
	QList<CArchiveBranch *> mBranches;
	CArchiveBranch *parentNode;

	QString mPath;
	QIcon mIcon;
	QString mSize;

	QStringList __nameFilters;
	bool __showHidden;
};

bool caseInsensitiveNameSort(CArchiveBranch *first, CArchiveBranch *second);
