/*
	*
	* This file is a part of CoreArchiver.
	* Archive manager for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#pragma once

#include <libarchiveqt.h>
#include "carchivebranch.h"

class CArchiveModel : public QAbstractItemModel {
	Q_OBJECT

public:
	explicit CArchiveModel();
	~CArchiveModel();

	/* When we start with a new archive */
	void setArchiveName(const QString &path);

	/* When we want to load an archive */
	void loadArchive(const QString &path);

	/* Children Info */
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;

	Qt::ItemFlags flags(const QModelIndex &) const;

	/* Display Info */
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

	/* Node Info */
	QModelIndex index(int row = 0, int column = 0, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex index(QString, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index = QModelIndex()) const;

	/* Children, fetchMore */
	bool hasBranches(const QModelIndex &index = QModelIndex()) const;
	void selectBranches(QStringList);

	/* Add (or remove) files to (from) the archive */
	void growBranch(QStringList);
	void chopBranch(QModelIndexList);

	/* Commit the changes, if any, to the archive on the disk */
	bool isArchiveModified();
	QStringList filePaths();

	/* FS Navigation */
	QString nodeName(const QModelIndex) const;
	QString nodePath(const QModelIndex) const;
	QString nodePath(QString) const;
	QFileInfo nodeInfo(const QModelIndex) const;

	void clear();

private:
	CArchiveBranch *tree;

	QString archiveName;

	/* Archive modified flag */
	bool mModified = false;

	/* Grow the tree */
	void growTree();

Q_SIGNALS:
	void archiveLoading();
	void archiveLoaded();
};
