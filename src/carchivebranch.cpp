/*
	*
	* This file is a part of CoreArchiver.
	* Archive manager for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#include <QVariant>
#include "carchivebranch.h"

CArchiveBranch::CArchiveBranch()
	: QObject()
	, parentNode(0)
	, mPath(QString())
	, mIcon(QIcon())
	, mSize(QString())
	, __showHidden(false)
{
}

CArchiveBranch::CArchiveBranch(const QString &name, QIcon icon, const QString &size, CArchiveBranch *parent)
	: QObject()
	, parentNode(parent)
	, mPath(name)
	, mIcon(icon)
	, mSize(size)
	, __showHidden(false)
{
}

CArchiveBranch::~CArchiveBranch()
{
	mBranches.clear();
}

int CArchiveBranch::branchCount()
{
	return mBranches.count();
}

void CArchiveBranch::clearBranches()
{
	mBranches.clear();
}

void CArchiveBranch::addBranch(CArchiveBranch *newNode)
{
	Q_FOREACH (CArchiveBranch *node, mBranches)
		if (node->name() == newNode->name()) {
			return;
		}

	mBranches << newNode;
}

void CArchiveBranch::removeBranch(CArchiveBranch *node)
{

	delete mBranches.takeAt(node->row());
}

CArchiveBranch *CArchiveBranch::branch(int row)
{
	return mBranches.at(row);
}

CArchiveBranch *CArchiveBranch::branch(const QString &name)
{
	Q_FOREACH (CArchiveBranch *node, mBranches)
		if (node->name() == name) {
			return node;
		}

	return new CArchiveBranch();
}

QList<CArchiveBranch *> CArchiveBranch::branches()
{
	return mBranches;
}

QString CArchiveBranch::name()
{
	return mPath;
}

QString CArchiveBranch::size()
{
	return mSize;
}

QIcon CArchiveBranch::icon()
{
	return mIcon;
}

QVariant CArchiveBranch::data(int role) const
{
	switch (role) {
		case Qt::DisplayRole:
			return mPath;

		case Qt::DecorationRole:
			return mIcon;

		case Qt::UserRole + 1:
			return mPath;

		default:
			return QVariant();
	}
}

bool CArchiveBranch::setData(int, QVariant)
{
	return true;
}

CArchiveBranch *CArchiveBranch::parent()
{
	return parentNode;
}

int CArchiveBranch::row()
{
	/* If mPath is not defined */
    if (not mPath.length()) {
		return -1;
	}

	if (parentNode) {
		return parentNode->mBranches.indexOf(this);
	}

	return 0;
}

void CArchiveBranch::sort()
{
	std::sort(mBranches.begin(), mBranches.end(), caseInsensitiveNameSort);
}

bool caseInsensitiveNameSort(CArchiveBranch *first, CArchiveBranch *second)
{
	QString name1 = first->name().toLower();
	QString name2 = second->name().toLower();

	return name1 < name2;
}
