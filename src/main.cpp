/*
    *
    * This file is a part of CoreArchiver.
    * Archive manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QApplication>
#include <QCommandLineParser>
#include <QFileInfo>

#include "corearchiver.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreArchiver");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CoreArchiver.desktop");
    app.setQuitOnLastWindowClosed(true);

    QCommandLineParser parser;
	parser.setApplicationDescription("A simple archive manager from C Suite.");
    parser.addHelpOption();
	parser.addVersionOption();
    parser.addPositionalArgument("files", "Files that you want to archive or extract", "files");
    parser.process(app);

    QStringList args = parser.positionalArguments();
    QStringList paths;

    Q_FOREACH(QString arg, args) {
        QFileInfo info(arg);
		paths.append(info.absoluteFilePath());
    }

	corearchiver carc;
	carc.show();

	qApp->processEvents();

	carc.sendFiles(paths);

    return app.exec();
}
