/*
    *
    * This file is a part of CoreArchiver.
    * Archive manager for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QFileDialog>
#include <QScroller>
#include <QDir>
#include <QMessageBox>

#include <libarchiveqt.h>

#include <cprime/filefunc.h>
#include <cprime/messageengine.h>

#include "settings.h"
#include "carchivemodel.h"
#include "corearchiver.h"
#include "ui_corearchiver.h"

/*
	*
	* Notes on UI
	*
	* I have made several changes to the UI to make it a bit more intuitive.
	* nameLE  -> CREATE:  Will show the full file path of the archive and formatsCB will be hidden.
	*            EXTRACT: Will show just the only file name (no suffix), and formatsCB will provide the suffix.
	* workDir -> CREATE:  Path where the archive will be saved
	*            EXTRACT: Destination folder where the archive will be extracted to
	* progressBar can show the progress of compressing and extracting archives. To be hidden when no work is being done.
	*
*/

corearchiver::corearchiver(QWidget *parent) :
	QWidget(parent)
	, ui(new Ui::corearchiver)
	, smi(new settings)
{
	ui->setupUi(this);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	loadSettings();
	startSetup();
}

corearchiver::~corearchiver()
{
	delete smi;
	delete ui;
}

void corearchiver::sendFiles(const QStringList &paths)
{
	if (!paths.count()) {
		ui->page->setCurrentIndex(0);
		return;
	}

	bool isArchive = false;

	if (paths.count() == 1) { // Check is this a archive file
		QFileInfo fi(paths.at(0));

		QStringList supported;
		supported << "7z" << "ar" << "cpio" << "iso" << "shar" << "tar" <<
				  "bz2" << "gz" << "xz" << "Z" << "xar" << "zip";

		if (supported.contains(fi.suffix(), Qt::CaseInsensitive)) {
			if (CPrime::FileUtils::exists(fi.absoluteFilePath())) {
				isArchive = true;
			} else {
				QMessageBox::warning(this, "Archive not exists",
									 "Given archive file does not exists.\nPlease provide a valid archive.");
				return;
			}
		}
	}

	ui->page->setCurrentIndex(1);

	if (isArchive) { // Trying to open archive file
		prepareLoadArchive();
		loadArchive(paths.at(0));
	} else {
		// Creating new archive
		prepareCreateArchive();
		loadFiles(paths);
	}
}

/**
 * @brief Loads application settings
 */
void corearchiver::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

    // get app's settings
    windowSize = smi->getValue("CoreArchiver", "WindowSize");
    windowMaximized = smi->getValue("CoreArchiver", "WindowMaximized");
}

void corearchiver::startSetup()
{
	QScroller::grabGesture(ui->archiveView, QScroller::LeftMouseButtonGesture);

	model = new CArchiveModel();

	// Setup the archive view model
	ui->archiveView->setModel(model);

	supportedFormats = QStringList() << ".7z" << ".ar" << ".cpio" << ".pax" <<
					   ".iso" << ".shar" << ".tar" << ".tar.bz2" <<
					   ".tar.gz" << ".tar.xz" << ".tar.Z" << "xar" << ".zip";

	ui->formatsCB->addItems(supportedFormats);

	connect(ui->back, &QToolButton::clicked, [ = ]() {
		model->clear();
		ui->page->setCurrentIndex(0);
	});

	connect(model, &CArchiveModel::archiveLoaded, ui->progressBar, &QProgressBar::hide);
	connect(ui->archiveView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &corearchiver::updateSelection);

	ui->page->setCurrentIndex(0);

	/* Hide the progress bar */
	ui->progressBar->hide();

	ui->archiveView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
	ui->archiveView->header()->setSectionResizeMode(1, QHeaderView::Fixed);
	ui->archiveView->header()->resizeSection(1, 200);
	ui->archiveView->setIconSize(listViewIconSize);

	this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        // all toolbuttons in icon mode in toolBar
        QList<QToolButton *> toolBtns = ui->toolBar->findChildren<QToolButton *>();

        Q_FOREACH (QToolButton *b, toolBtns) {
            if (b) {
                b->setIconSize(toolsIconSize);
                b->setToolButtonStyle(Qt::ToolButtonIconOnly);
            }
        }

    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

}

void corearchiver::showArchiveMessage()
{
	ui->progressBar->hide();

    CPrime::MessageEngine::showMessage("cc.cubocore.CoreArchiver", "CoreArchiver",
										 "Archived Successfully", "No issues, everthing went smoothly");

	/* Re-enable the UI */
	setEnabled(true);

	prepareLoadArchive();

	/* Open the just created archive for viewing */
	loadArchive(QDir(ui->locationLE->text()).filePath(ui->nameLE->text() + ui->formatsCB->currentText()));
}

void corearchiver::showExtractMessage()
{
	ui->progressBar->hide();

    CPrime::MessageEngine::showMessage("cc.cubocore.CoreArchiver", "CoreArchiver",
										 "Extracted Successfully...", "No issues, everthing went smoothly");

	/* Re-enable the UI */
	setEnabled(true);
}

void corearchiver::updateSelection(const QItemSelection &, const QItemSelection &)
{
	updateActions();
}

void corearchiver::updateActions()
{
	bool canCreateArchive = false;

	if (model->rowCount()) {
		canCreateArchive = true;
	}

	ui->arcAction->setEnabled(canCreateArchive);

	bool createNewArchive = false;

	if (ui->arcAction->text() == "Compress") {
		createNewArchive = true;
		ui->formatsCB->setCurrentText(".zip");
	}

	ui->addFiles->setVisible(createNewArchive);
	ui->addFolders->setVisible(createNewArchive);
	ui->removeFile->setVisible(createNewArchive);
	ui->formatsCB->setVisible(createNewArchive);
	ui->nameLE->setReadOnly(!createNewArchive);

	bool canRemove = false;

	if (ui->archiveView->selectionModel()->selectedIndexes().count()) {
		canRemove = true;
	}

	ui->removeFile->setEnabled(canRemove);
}

void corearchiver::prepareCreateArchive()
{
	/* Work dir: Show the current directory */
	ui->locationLE->setText(QDir::currentPath());

	/* arcAction button text */
	ui->arcAction->setText("Compress");
	ui->arcAction->setIcon(QIcon::fromTheme("archive-insert"));

	/* Hide the progressBar */
	ui->progressBar->hide();

	updateActions();
}

void corearchiver::prepareLoadArchive()
{
	/* Work dir: Show the current directory */
	ui->locationLE->setText(QDir::currentPath());

	/* arcAction button text */
	ui->arcAction->setText("Extract");
	ui->arcAction->setIcon(QIcon::fromTheme("archive-extract"));

	/* Show the progressBar */
	ui->progressBar->show();
	ui->progressBar->setRange(0, 0);

	updateActions();
}

void corearchiver::loadArchive(const QString &fileName)
{
	/* We will put the full archive path in nameLE, and make it read only */
	ui->nameLE->setText(fileName);

	model->clear();
	model->loadArchive(fileName);

	updateActions();
}

void corearchiver::loadFiles(const QStringList &files)
{
	QStringList paths;
	// Check whether files exists or not
	Q_FOREACH (auto file, files) {
		if (CPrime::FileUtils::exists(file)) {
			paths << file;
		}
	}

	ui->nameLE->setText("Archive");

	if (paths.count() != files.count()) {
		QMessageBox::warning(this, "Files not exists",
							 "There are some files given that are not exists.");
	}

	model->clear();
	model->growBranch(paths);

	updateActions();
}

bool corearchiver::checkForSpace(const QString &path, const quint64 size)
{
    qDebug ()<< size << path;
    // Get the storage information for the specified path
    QFileInfo sstorage(path);
    bool safeToProceed = false;

    if (not sstorage.isWritable()) {
        qDebug() << "not writable";
        CPrime::MessageEngine::showMessage("cc.cubocore.CoreArchiver", "CoreArchiver",
                                           "Extraction paused", "The destination is read-only, select a different destination");
        safeToProceed = false;
    } else {
        QStorageInfo storage(path);
        qDebug() << "Available size:" << storage.bytesAvailable() << "bytes";
        quint64 availableStorage = storage.bytesAvailable();
        if(availableStorage < size){
            CPrime::MessageEngine::showMessage("cc.cubocore.CoreArchiver", "CoreArchiver",
                                               "Extraction paused", "Not enough free space, select a different destination");
            safeToProceed = false;
        } else {
            safeToProceed = true;
        }
    }

    return safeToProceed;
}

void corearchiver::compress()
{
    qint64 totalSize = 0;
    foreach (const QString &fileName, model->filePaths()) {
        QFileInfo fileInfo(fileName);
        if (fileInfo.exists()) {
            totalSize += fileInfo.size();
        } else {
            qDebug() << "File:" << fileName << "does not exist.";
        }
    }

    // check for engough space, else don't proceed
    if(not checkForSpace(ui->locationLE->text(), totalSize)){
        return;
    }

	/* The archive will always be created at the current location. */
	QString archive = QDir(ui->locationLE->text()).filePath(ui->nameLE->text() + ui->formatsCB->currentText());

	/* LibArchiveQt object to create the archive */
	LibArchiveQt *arc = new LibArchiveQt(archive);
	connect(arc, &LibArchiveQt::progress, ui->progressBar, &QProgressBar::setValue);
	connect(arc, &LibArchiveQt::jobComplete, this, &corearchiver::showArchiveMessage);

	/* Use the path common to all the input files as the relative path */
	arc->updateInputFiles(model->filePaths(), LibArchiveQt::CommonRelativePath);


	/* ProgressBar will have a range of 0 -> 100 during extraction */
	ui->progressBar->setRange(0, 100);
	ui->progressBar->show();

	/* Disable the UI */
	setDisabled(true);

	/* Start the archive extraction */
	arc->createArchive();
}

void corearchiver::extract()
{
	QString destP = ui->locationLE->text();

    LibArchiveQt *arc = new LibArchiveQt(ui->nameLE->text());

    ArchiveEntries entries = arc->listArchive();
    quint64 totalSize = 0;
    for( ArchiveEntry *entry: entries ) {
        totalSize += entry->size;
    }

    // check for engough space, else don't proceed
    if(not checkForSpace(destP, totalSize)){
        return;
    }

	connect(arc, &LibArchiveQt::progress, ui->progressBar, &QProgressBar::setValue);
	connect(arc, &LibArchiveQt::jobComplete, this, &corearchiver::showExtractMessage);

	// LibArchiveQt will create destP if it does not exist
	arc->setDestination(destP);

	/* ProgressBar will have a range of 0 -> 100 during extraction */
	ui->progressBar->setRange(0, 100);
	ui->progressBar->show();

	/* Disable the UI */
	setDisabled(true);

	/* Start the archive extraction */
	arc->extractArchive();
}

void corearchiver::on_openArchive_clicked()
{
	/* Get our archive name */
	QString filterList = "Archives (*" + supportedFormats.join(" *") + ");;All files (*.*)";

	QString fileName = QFileDialog::getOpenFileName(
						   this,
						   "CoreArchiver | Open archive",
						   QDir::homePath(),
						   filterList
					   );

	/* If the user has not selected any archive, return to welcome page */
	if (fileName.isEmpty()) {
		ui->page->setCurrentIndex(0);
		return;
	}

	ui->page->setCurrentIndex(1);

	prepareLoadArchive();
	loadArchive(fileName);
}

void corearchiver::on_createArchive_clicked()
{
	ui->nameLE->setText("Archive");

	ui->page->setCurrentIndex(1);
	prepareCreateArchive();
}

void corearchiver::on_addFiles_clicked()
{
	QStringList list = QFileDialog::getOpenFileNames(this, "Add files", QDir::homePath(), "*");

	/* Start adding the files to the archive */
	model->growBranch(list);

	updateActions();
}

void corearchiver::on_addFolders_clicked()
{
	QString path = QFileDialog::getExistingDirectory(this, "Add Folder", QDir::homePath());

	if (CPrime::FileUtils::exists(path)) {
		model->growBranch({path});
	}

	updateActions();
}

void corearchiver::on_removeFile_clicked()
{
	QModelIndexList indexList = ui->archiveView->selectionModel()->selectedRows(0);

	if (indexList.count()) {
		model->chopBranch(indexList);
	}

	updateActions();
}

void corearchiver::on_arcAction_clicked()
{
	if (ui->arcAction->text() == "Compress") {
		QString archive = QDir(ui->locationLE->text()).filePath(ui->nameLE->text() + ui->formatsCB->currentText());

		if (QFile(archive).exists()) {
            CPrime::MessageEngine::showMessage("cc.cubocore.CoreArchiver", "CoreArchiver",
												 "Cannot create archive file exists",
												 "A file with that name already exists.\nPlease give a new name.");
			return;
		}

		compress();
	} else {
		extract();
	}
}

void corearchiver::on_locationTB_clicked()
{
	QString location = QFileDialog::getExistingDirectory(
						   this,
						   "CoreArchiver | Set extraction location",
						   QDir::homePath()
					   );

	if (not location.isEmpty()) {
		ui->locationLE->setText(location);
	}
}

void corearchiver::closeEvent(QCloseEvent *event)
{
    smi->setValue("CoreArchiver", "WindowSize", this->size());
    smi->setValue("CoreArchiver", "WindowMaximized", this->isMaximized());

    event->accept();
}
